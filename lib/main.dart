import 'dart:io';

import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_wan/common/application.dart';
import 'package:flutter_wan/event/theme_change_event.dart';
import 'package:flutter_wan/res/colors.dart';
import 'package:flutter_wan/ui/splash_screen.dart';
import 'package:flutter_wan/utils/theme_util.dart';

import 'common/common.dart';
import 'common/router_config.dart';
import 'net/dio_manager.dart';
import 'utils/sp_util.dart';

/// 在拿不到context 的地方通过 navigatorKey 进行路由跳转

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SPUtil.getInstance();

  await getTheme();

  runApp(MyApp());

  if(Platform.isAndroid){
    // 设置 android 状态栏为透明的沉浸，写在组件渲染之后
    // 是为了在渲染后进行 set 赋值，覆盖状态栏，写在渲染之前 MaterialApp 组件会覆盖掉这个值
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }

}

///获取主题
Future<Null> getTheme() async {
  //是否是夜间模式
  bool dark = SPUtil.getBool(Constants.DARK_KEY,defValue: false);
  ThemeUtils.dark = dark;
  if(!dark){
    String themeColorKey = SPUtil.getString(Constants.THEME_COLOR_KEY,defValue: "redAccent");
    if(themeColorMap.containsKey(themeColorKey)){
      ThemeUtils.currentThemColor = themeColorMap[themeColorKey];
    }
  }
}

class MyApp extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp>{

  /** 主题模式 */
  ThemeData themeData;


  @override
  void initState() {
    super.initState();
    _initAsync();
    Application.eventBus = new EventBus();
    themeData = ThemeUtils.getThemeData();
    this.registerThemeEvent();
  }

  void _initAsync() async{
//    await User().getUserInfo();
    await DioManager.init();
  }
  
  /// 注册 主题改变事件
  void registerThemeEvent(){
    Application.eventBus
        .on<ThemeChangeEvent>()
        .listen((onData) => this.changeTheme(onData));
  }

  void changeTheme(ThemeChangeEvent onData) async{
    setState(() {
      themeData = ThemeUtils.getThemeData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppConfig.appName,
      debugShowCheckedModeBanner: AppConfig.isDebug,
      theme: themeData,
      routes: Router.generateRoute(),
      navigatorKey: navigatorKey,
      home: new SplashScreen(),
    );
  }

  @override
  void dispose() {
    super.dispose();
    Application.eventBus.destroy();
  }


}