
class Constants{

  static const int STATUS_SUCCESS = 0;

  static const String DARK_KEY = "dark_key";

  static const THEME_COLOR_KEY = "theme_color_key";



}

class AppConfig {

  static const String appName = "玩 Android";
  static const String version = "1.0.0";
  static const int versionCode = 1;
  static const bool isDebug = true;

}