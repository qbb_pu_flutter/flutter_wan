

import 'package:flutter/cupertino.dart';
import 'package:flutter_wan/ui/splash_screen.dart';

class RouterName {
  static const String splash = "splash";
}

class Router {

  static Map<String,WidgetBuilder> generateRoute() {
    Map<String, WidgetBuilder> routes = {
      RouterName.splash: (context) => new SplashScreen()
    };
    return routes;
  }
}