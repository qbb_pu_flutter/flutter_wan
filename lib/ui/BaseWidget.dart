import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wan/utils/utils.dart';

/// Author：Qianbingbing
/// Create at 2019-12-30 13:09
/// Description： 通用 widget
abstract class BaseWidget extends StatefulWidget {
  BaseWidgetState baseWidgetState;

  @override
  State<StatefulWidget> createState() {
    baseWidgetState = attachState();
    return baseWidgetState;
  }

  BaseWidgetState attachState();

}


abstract class BaseWidgetState<T extends BaseWidget> extends State<T>
    with AutomaticKeepAliveClientMixin {

  bool _isAppBarShow = true;

  /// 错误界面
  bool _isErrorWidgetShow = false;
  String _errorContentMsg = "网路请求失败，请检查您的网路";
  String _errorImgPath = Utils.getImgPath("ic_empty");

  /// 正在加载界面
  bool _isLoadingWidgetShow = false;

  /// 空界面
  bool _isEmptyWidgetShow = false;
  String _emptyContentMsg = "暂无数据";
  String _emptyImgPath = Utils.getImgPath("ic_empty");

  /// 内容界面
  bool _isShowContent = false;

  /// 错误界面和空界面的字体粗度
  FontWeight _fontWeight = FontWeight.w600;

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: _attachBaseAppBar(),
      body: Container(
        child: Stack(
          children: <Widget>[
            _attachBaseContentWidget(context),
            _attachBaseErrorWidget(),
            _attachBaseLoadingWidget(),
            _attachBaseEmptyWidget()
          ],
        ),
      ),
      floatingActionButton: fabWidget(),
    );
  }

  ///悬浮按钮
  Widget fabWidget(){
    return null;
  }

  /// 导航栏 Appbar
  AppBar attachAppBar();

  Widget attachContentWidget(BuildContext context);

  void onClickErrorWidget();

  /// 导航栏 AppBar
  PreferredSizeWidget _attachBaseAppBar() {
    return PreferredSize(
        child: Offstage(
          offstage: !_isAppBarShow,
          child: attachAppBar(),
        ),
        preferredSize: Size.fromHeight(56)
    );
  }

  ///内容界面
  Widget _attachBaseContentWidget(BuildContext context) {
    return Offstage(
      offstage: !_isShowContent,
      child: attachContentWidget(context),
    );
  }

  ///错误界面
  Widget _attachBaseErrorWidget() {
    return Offstage(
      offstage: !_isErrorWidgetShow,
      child: attachErrorWidget(),
    );
  }

  ///暴露的错误的界面
  Widget attachErrorWidget() {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 80),
      width: double.infinity,
      height: double.infinity,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage(_errorImgPath),
              width: 120,
              height: 120,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Text(
                _errorContentMsg,
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: _fontWeight
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: OutlineButton(
                child: Text(
                  "重新加载",
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: _fontWeight
                  ),
                ),
                onPressed: () => { onClickErrorWidget() },
              ),
            )
          ],
        ),
      ),
    );
  }

  /// 正在加载界面
  Widget _attachBaseLoadingWidget(){
    return Offstage(
      offstage: !_isLoadingWidgetShow,
      child: attachLoadingWidget(),
    );
  }

  /// 默认的正在加载界面的方法
  Widget attachLoadingWidget(){
    return Center(
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
      ),
    );
  }

  /// 空数据界面
  Widget _attachBaseEmptyWidget(){
    return Offstage(
      offstage: !_isEmptyWidgetShow,
      child: attachEmptyWidget(),
    );
  }

  /// 默认的空数据界面
  Widget attachEmptyWidget(){
    return Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 100),
      width: double.infinity,
      height: double.infinity,
      child: Center(
        child: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(
                color: Colors.black12,
                image: AssetImage(_emptyImgPath),
                width: 150,
                height: 150,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Text(
                  _emptyContentMsg,
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: _fontWeight
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  /// 设置错误信息
  Future setErrorContent(String content) async{
    if (content != null) {
      setState(() {
        _errorContentMsg = content;
      });
    }
  }

  /// 设置空界面信息
  Future setEmptyContent(String content) async{
    if (content != null) {
      setState(() {
        _emptyContentMsg = content;
      });
    }
  }

  Future setAppBarVisible(bool visible) async{
    setState(() {
      _isAppBarShow = visible;
    });
  }

  /// 显示内容
  Future showContent() async{
    setState(() {
      _isShowContent = true;
      _isEmptyWidgetShow = false;
      _isLoadingWidgetShow = false;
      _isErrorWidgetShow = false;
    });
  }

  /// 显示正在加载
  Future showLoading() async{
    setState(() {
      _isShowContent = false;
      _isEmptyWidgetShow = false;
      _isLoadingWidgetShow = true;
      _isErrorWidgetShow = false;
    });
  }

  /// 显示空数据界面
  Future showEmpty() async{
    setState(() {
      _isShowContent = false;
      _isEmptyWidgetShow = true;
      _isLoadingWidgetShow = false;
      _isErrorWidgetShow = false;
    });
  }

  /// 显示错误界面
  Future showError() async{
    setState(() {
      _isShowContent = false;
      _isEmptyWidgetShow = false;
      _isLoadingWidgetShow = false;
      _isErrorWidgetShow = true;
    });
  }
}