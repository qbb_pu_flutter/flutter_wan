
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter_wan/data/api/apis.dart';
import 'package:flutter_wan/net/interceptors/LogInterceptors.dart';

Dio _dio = Dio();   /// 使用默认配置

Dio get dio => _dio;


class DioManager {

  static Future init() async {
    dio.options.baseUrl = Apis.BASE_HOST;
    dio.options.contentType = ContentType.parse("application/x-www-form-urlencoded").toString();
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
    (client) {
      client.badCertificateCallback = (X509Certificate cert, String host, int port){
        return true;
      };
    };

    dio.interceptors.add(LogInterceptors());
  }
}