import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ProgressView.dart';

/// 自定义带有缓存的 Image
class CustomCachedImage extends StatelessWidget {
  final String imageUrl;
  final BoxFit fit;

  CustomCachedImage({Key key, @required this.imageUrl, this.fit = BoxFit.cover})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return imageUrl.isNotEmpty
    ? CachedNetworkImage(
      fit: fit,
      imageUrl: imageUrl,
      placeholder: (context, url) => ProgressView(),
      errorWidget: (context,url,error) => Icon(Icons.error),
    )
    : Container();
  }
}
