
int maxLength =340;

void printLong(String log){
  if(log.length < maxLength){
    print(log);
  }else{
    while(log.length > maxLength) {
      printLong(log.substring(0,maxLength));
      log = log.substring(maxLength);
    }
    print(log);
  }
}