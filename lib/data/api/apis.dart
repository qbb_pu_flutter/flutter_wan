

class Apis{
  static const String BASE_HOST = "https://www.wanandroid.com/";

  /// 首页轮播
  static const String HOME_BANNER = "banner/json";
  /// 首页列表
  static const String HOME_ARTICLE_LIST = "article/list";
  /// 首页置顶文章列表
  static const String HOME_TOP_ARTICLE_LIST = "article/top/json";



  static const String PROJECT_ARTICLE_LIST = "project/list";
}